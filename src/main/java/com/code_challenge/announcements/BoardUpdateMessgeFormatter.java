package com.code_challenge.announcements;

import com.code_challenge.model.ArrivalModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Slf4j
@Component("boardFormatter")
public class BoardUpdateMessgeFormatter implements MessgeFormatter {

    public String prepareAnnouncement(ArrivalModel arrivalModel) {
        log.debug("expected arrival: " + arrivalModel.getExpectedArrival().format(DateTimeFormatter.ISO_DATE_TIME));
        log.debug("current time    : " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        return
                "Announcement time: " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
                + " | Train arrives in "
                + LocalDateTime.now().until(arrivalModel.getExpectedArrival(), ChronoUnit.MINUTES)
                + " minutes";
    }

}
