package com.code_challenge.announcements;

import com.code_challenge.model.ArrivalModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Component("arrivalFormatter")
public class ArrivalMessgeFormatter implements MessgeFormatter {

    public String prepareAnnouncement(ArrivalModel arrivalModel) {
        log.debug("expected arrival: " + arrivalModel.getExpectedArrival().format(DateTimeFormatter.ISO_DATE_TIME));
        log.debug("current time    : " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        return
                "Announcement time: " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
                + " | The train for "
                + arrivalModel.getExpectedArrival()
                + " has arrived";
    }

}
