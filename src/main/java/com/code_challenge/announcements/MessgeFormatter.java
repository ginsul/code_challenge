package com.code_challenge.announcements;

import com.code_challenge.model.ArrivalModel;

public interface MessgeFormatter {

    String prepareAnnouncement(ArrivalModel arrivalModel);

}
