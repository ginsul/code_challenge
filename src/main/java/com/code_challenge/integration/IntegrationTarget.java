package com.code_challenge.integration;

import java.io.IOException;
import java.util.List;

public interface IntegrationTarget {

    /*
     *  Triggers voice announcement upon train arrival
     */
    void sendArrival(String arrival);

    /*
     *  Updates the arrivals board with the given arrival information
     */
    void sendFollowingArrivals(List<String> arrivals);

}
