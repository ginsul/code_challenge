package com.code_challenge.integration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class DefaultIntegrationTarget implements IntegrationTarget {

    @Override
    public void sendArrival(String arrival) {
        log.debug("ANNOUNCEMENT: " + arrival);
    }

    @Override
    public void sendFollowingArrivals(List<String> arrivals) {
        arrivals.stream().forEach(arrival -> {
            log.debug("BORAD: " + arrival);
        });
    }
}
