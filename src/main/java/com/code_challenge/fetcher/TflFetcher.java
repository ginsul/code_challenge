package com.code_challenge.fetcher;

import org.springframework.scheduling.annotation.Scheduled;

public interface TflFetcher {

    void fetch();

}
