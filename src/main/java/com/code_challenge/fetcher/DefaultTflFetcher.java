package com.code_challenge.fetcher;

import com.code_challenge.fetcher.event.FetcherEvent;
import com.code_challenge.model.ArrivalModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class DefaultTflFetcher implements TflFetcher {

    @Value("${fetcher.auth.appId}")
    String appId;

    @Value("${fetcher.auth.appKey}")
    String appKey;

    @Value("${fetcher.endpoint.fetchUrl}")
    String fetchUrl;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public DefaultTflFetcher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Scheduled(fixedRate = 60000)
    @Override
    public void fetch() {
        log.debug("fetching data from: " + fetchUrl);
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("app_id", appId);
        headers.set("app_key", appKey);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<List<ArrivalModel>> response = restTemplate.
                exchange(fetchUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<ArrivalModel>>() {
                });

        publishFetchEvent(response.getBody());
    }

    private void publishFetchEvent(List<ArrivalModel> fetchedArrivals) {
        FetcherEvent customSpringEvent = new FetcherEvent(this, fetchedArrivals);
        applicationEventPublisher.publishEvent(customSpringEvent);
    }
}
