package com.code_challenge.fetcher.event;

import com.code_challenge.model.ArrivalModel;
import org.springframework.context.ApplicationEvent;

import java.util.List;

public class FetcherEvent extends ApplicationEvent {

    private List<ArrivalModel> message;

    public FetcherEvent(Object source, List<ArrivalModel> message) {
        super(source);
        this.message = message;
    }

    public List<ArrivalModel> getMessage() {
        return message;
    }
}