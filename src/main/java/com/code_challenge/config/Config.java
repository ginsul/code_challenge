package com.code_challenge.config;

import com.code_challenge.fetcher.TflFetcher;
import com.code_challenge.integration.IntegrationTarget;
import com.code_challenge.scheduler.AnnouncementScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
public class Config {

    @Autowired
    public TflFetcher tflFetcher;

    @Autowired
    public IntegrationTarget integrationTarget;

    @Autowired
    @Qualifier("defaultBoardUpdater")
    public AnnouncementScheduler boardUpdaterScheduler;

    @Autowired
    @Qualifier("defaultArrivalsAnnouncer")
    public AnnouncementScheduler arrivalScheduler;

    @Bean(name = "applicationEventMulticaster")
    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster
                = new SimpleApplicationEventMulticaster();

        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return eventMulticaster;
    }
}
