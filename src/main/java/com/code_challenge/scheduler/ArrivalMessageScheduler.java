package com.code_challenge.scheduler;

import com.code_challenge.announcements.MessgeFormatter;
import com.code_challenge.fetcher.event.FetcherEvent;
import com.code_challenge.integration.IntegrationTarget;
import com.code_challenge.model.ArrivalModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service("defaultArrivalsAnnouncer")
public class ArrivalMessageScheduler implements AnnouncementScheduler {

    private ArrivalModel firstonListArrival = null;
    private ArrivalModel targetArrival = null;
    private String announcedArrivalId = "-1";

    private final IntegrationTarget integrationTarget;

    private final MessgeFormatter messgeFormatter;

    @Autowired
    public ArrivalMessageScheduler(IntegrationTarget integrationTarget, @Qualifier("arrivalFormatter") MessgeFormatter messgeFormatter) {
        this.integrationTarget = integrationTarget;
        this.messgeFormatter = messgeFormatter;
    }

    @Scheduled(fixedRate = 1000)
    public void checkAndAnnounce() {
        if (targetArrival != null && targetArrival.getExpectedArrival().isBefore(LocalDateTime.now()) && !targetArrival.getId().equals(announcedArrivalId)) {
            integrationTarget.sendArrival(messgeFormatter.prepareAnnouncement(targetArrival));
            announcedArrivalId = targetArrival.getId();
        }
    }

    @Override
    public void onApplicationEvent(FetcherEvent event) {
        List<ArrivalModel> arrivalsList = event.getMessage();

        arrivalsList.stream().limit(1).forEach(arrival -> {
            if (firstonListArrival != null) {
                targetArrival = firstonListArrival;
            } else {
                targetArrival = arrival;
            }

            firstonListArrival = arrival;
        });
    }

}
