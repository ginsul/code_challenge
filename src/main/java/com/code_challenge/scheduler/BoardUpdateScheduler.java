package com.code_challenge.scheduler;

import com.code_challenge.announcements.MessgeFormatter;
import com.code_challenge.fetcher.event.FetcherEvent;
import com.code_challenge.integration.IntegrationTarget;
import com.code_challenge.model.ArrivalModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service("defaultBoardUpdater")
public class BoardUpdateScheduler implements AnnouncementScheduler {

    private final IntegrationTarget integrationTarget;

    private final MessgeFormatter messgeFormatter;

    @Autowired
    public BoardUpdateScheduler(IntegrationTarget integrationTarget, @Qualifier("boardFormatter") MessgeFormatter messgeFormatter) {
        this.integrationTarget = integrationTarget;
        this.messgeFormatter = messgeFormatter;
    }

    @Override
    public void onApplicationEvent(FetcherEvent event) {
        List<ArrivalModel> arrivalsList = event.getMessage();

        integrationTarget.sendFollowingArrivals(
                arrivalsList.stream().limit(3).map(arrival -> messgeFormatter.prepareAnnouncement(arrival)).collect(Collectors.toList()));

    }

}
