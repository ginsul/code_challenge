package com.code_challenge.scheduler;

import com.code_challenge.fetcher.event.FetcherEvent;
import org.springframework.context.ApplicationListener;

public interface AnnouncementScheduler extends ApplicationListener<FetcherEvent> {

}
